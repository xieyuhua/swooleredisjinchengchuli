# swoole-Redis多进程处理，

#### 介绍


1. 这里是列表文本 本项目是基于swoole和php写的一个内存常驻型的，队列消耗系统，适用于执行任务耗时很长，处理数据量多的系统，本系统性能强悍，唯一不足就是在进程切换耗费CPU资- 这里是列表文本源，和任务过长阻塞io资源消耗，，，后面可以改进使用协程，来提高性能，，但是Redis性能存储服务器也有待提高。


#### 软件架构

1. swoole
1. redis
#### 安装教程


1. php 
1. swoole 
1. redis


#### 使用说明
1、多进程版本

```
$rand                  =mt_rand(0, 100);
$delay                 = 0;
$priority              =BaseTopicQueue::HIGH_LEVEL_1;
$jobExtras['delay']    =$delay;
$jobExtras['priority'] =$priority;
$job                   =new JobObject('Redisname', '\ap\index\Controller\JobAction', 'test1', ['参2','参1'], $jobExtras);
$result                =$queue->push('Redisname', $job, 1, 'json');
```


2、协程版本
```
<?php
require  'Loader.php';
use Yurun\Swoole\CoPool\CoPool;
use Yurun\Swoole\CoPool\Interfaces\ICoTask;
use Yurun\Swoole\CoPool\Interfaces\ITaskParam;
ini_set('memory_limit','1024M');
Swoole\Runtime::enableCoroutine();

class Demo {
    protected $server;
    protected $pool;
    protected $i = 0;
    public function __construct() {
        $this->server = new Swoole\Http\Server("0.0.0.0", 9501);
        $this->server->set(array(
            'worker_num' => 4,
            'max_request' => 1000,
            'reload_async' => true,
            'max_wait_time' => 30,
        ));
        $this->server->on('Start', function ($server) {});
        $this->server->on('ManagerStart', function ($server) {});
        $this->server->on('WorkerStart', array($this, 'onWorkerStart'));
        $this->server->on('WorkerStop', function ($server, $worker_id) {});
        $this->server->on('open', function ($server, $request) {});
        $this->server->on('Request', array($this, 'onRequest'));
        $this->server->start();
    }

    public function onWorkerStart($server, $worker_id) {

        echo '********************onWorkerStart: '.$worker_id.' *********************'.PHP_EOL;
        /*
          CoPool(50, 1024*10   50：协程，    队列1024*10
        */

        $this->pool = new CoPool(200, 1024*10,
            // 定义任务匿名类，当然你也可以定义成普通类，传入完整类名
            new class implements ICoTask {
                /**
                 * 执行任务
                 *
                 * @param ITaskParam $param
                 * @return mixed
                 */
                public function run(ITaskParam $param) {
                    //携程Redis数据库连接
                    $redis = new Swoole\Coroutine\Redis();
                    $redis->connect('127.0.0.1', 6379);
                    $val = $redis->get('key');


                    //携程mysql数据库连接
                    $swoole_mysql = new Swoole\Coroutine\MySQL();
                    $swoole_mysql->connect([
                        'host' => '127.0.0.1',
                        'port' => 3306,
                        'user' => 'xieyuhua',
                        'password' => 'xieyuhua',
                        'database' => 'bkapp',
                    ]);
                    $res = $swoole_mysql->query("INSERT INTO `bkapp`.`app_view` (`date`, `goods_code`, `goods_views`, `view_account`, `view_detail`) VALUES ('1', '1', '1', '1', '1')");

                    // 模拟I/O耗时挂起
                    usleep(mt_rand(100, 300) * 10000);

                    //释放数据库地址连接
                    unset($swoole_mysql);


                    echo ($param->getData()).PHP_EOL.date('Y-m-d h:i:s'), PHP_EOL;
                    return $param->getData();
                    // 返回任务执行结果，非必须
                }
            }
        );
        //启动协程
        $this->pool->run();
    }

    public function onRequest($request, $response) {

        echo '********************onRequest_start*********************'.PHP_EOL;
        $i = $this->i = $this->i+1;
        $pool = $this->pool;
        // $result = $this->pool->addTask('11010');//// 增加任务，并挂起协程等待返回任务执行结果

        //添加携程支持，，大数据来时候可以实现高并发，，大于固定最大可用进程时候携程挂起，，，，不允许携程阻，
        go(function() use($i, $pool) {
            $result = $pool->addTask($this->i, function(ITaskParam $param, $data) {
                // 异步回调
            });
            echo 'addTask:' . $i, PHP_EOL;
        });
        $response->end("<h1>hello swoole</h1>");
        // $pool->stop();
    }
}
//启动协程
new Demo();
```









命令行执行:php swoole-job


